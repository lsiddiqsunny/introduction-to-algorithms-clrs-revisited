# Chapter 3 : Growth of Functions
## Asymptotic notation
The notations we use to describe the asymptotic running time of an algorithm
are defined in terms of functions whose domains are the set of natural numbers
**N ={1,2,3,....}**

<img src="assests/1.png">

## Standard notations and common functions

**Monotonicity**
A function f(n) is **monotonically increasing** if m <= n implies f(m)<= f(n)
Similarly, it is **monotonically decreasing** if m <= n implies f(m)>= f(n). A
function f .n/ is **strictly increasing** if m < n implies f(m)< f(n) and **strictly decreasing** if m < nimplies f(m)> f(n)