def Binary_search(A,v,p,r):
    if p>=r and v!=A[p]:
        return -1
    j=p+(r-p)//2
    if v==A[j]:
        return j
    else:
        if v<A[j]:
            return Binary_search(A,v,p,j-1)
        else :
            return Binary_search(A,v,j+1,r)
A=[1,2,3,4,5,6]
pos=Binary_search(A,2,0,len(A)-1)
if pos==-1:
    print("Not found")
else :
    print("Found")

pos=Binary_search(A,10,0,len(A)-1)
if pos==-1:
    print("Not found")
else :
    print("Found")