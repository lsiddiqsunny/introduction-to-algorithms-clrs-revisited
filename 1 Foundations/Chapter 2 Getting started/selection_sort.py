def find_min(A,i,n):
    minimum=i
    for j in range(i,n):
        if A[minimum]>A[j]:
            minimum=j
    return minimum

def selection_sort(A):
    for i in range(0,len(A)):
        j=find_min(A,i,len(A))
        temp=A[j]
        A[j]=A[i]
        A[i]=temp
    
A=[5,2,4,6,1,3]
print("Before sorting :")
print(A)
selection_sort(A)
print("After sorting :")
print(A)