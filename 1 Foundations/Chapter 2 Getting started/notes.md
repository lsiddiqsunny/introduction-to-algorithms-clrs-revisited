# Chapter 2 : Getting started

This chapter will familiarize you with the framework we shall use throughout thebook to think about the design and analysis of algorithms.

## Insertion Sort

It is *in place* sorting algorithm.

In place means : it rearranges thenumbers within the array A, with at most a constant number of them stored outsidethe array at any time.

### Loop invariants

There are three steps in loop variants :

*Initialization:* It is true prior to the first iteration of the loop.

*Maintenance:* If it is true before an iteration of the loop, it remains true before thenext iteration.

*Termination:* When the loop terminates, the invariant gives us a useful propertythat helps show that the algorithm is correct

First two steps are like mathematical induction steps.One is base case and another one is induction steps.

## Analyzing Algorithm

We can analyze : *Best case*,*worst case* and *avarage case*

we shall usually concentrate  on finding only the worst-case running time, that is, the longest running time for anyinput of size n.
We give *three reasons* for this orientation.

1. The worst-case running time of an algorithm gives us an upper bound on the running time for any input.Knowing it provides a guarantee that the algorithm will never take any longer.We need not make some educated guess about therunning time and hope that it never gets much worse.

2. For some algorithms, the worst case occurs fairly often. For example, in search-ing a database for a particular piece of information, the searching algorithm’s worst case will often occur when the information is not present in the database. In some applications, searches for absent information may be frequent.

3. The “average case” is often roughly as bad as the worst case. Suppose that we randomly choose n numbers and apply insertion sort. How long does it take to determine where in subarray A.The resulting average-case running time turns out to be aquadratic function of the input size, just like the worst-case running time

## Designing Algorithm

We’ll use divide-and-conquer to design a sorting algorithm whose worst-case running time is muchless than that of insertion sort.

### The divide and conquer approach

The divide-and-conquer paradigm involves three steps at each level of the recursion:

1. *Divide:* the problem into a number of subproblems that are smaller instances of the same problem.
2. *Conquer:* the subproblems by solving them recursively. If the subproblem sizes are small enough, however, just solve the subproblems in a straight forward manner.
3. *Combine:* the solutions to the subproblems into the solution for the original problem
